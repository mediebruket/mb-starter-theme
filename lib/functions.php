<?php

/*-------------------------------------------------------------------------------------------*/
/* Log to debug.log */
/*-------------------------------------------------------------------------------------------*/
if(!function_exists('_log')){
  function _log( $message ) {
    if( WP_DEBUG === true ){
      if( is_array( $message ) || is_object( $message ) ){
        error_log( print_r( $message, true ) );
      } else {
        error_log( $message );
      }
    }
  }
}

/*-------------------------------------------------------------------------------------------*/
/* Assets functions */
/*-------------------------------------------------------------------------------------------*/
function get_stylesheets($assets = array()) {

  if($_SERVER['SERVER_NAME'] == MB_DEV_SERVER_NAME && !isset($_GET["prod"])) {
    $html = '';
    if (empty($assets)) {
      $html .= '<link rel="stylesheet" href="http://'.MB_ASSETS_URL.'/assets/style.css" type="text/css" />' . "\n";

      if ( isset($_GET['print']) )
        $html .= '<link rel="stylesheet" href="http://'.MB_ASSETS_URL.'/assets/print.css" type="text/css" media="print,screen" />' . "\n";

    }
    else {
      foreach ($assets as $asset) {
        $html .= '<link rel="stylesheet" href="http://'.MB_ASSETS_URL.'/assets/'.$asset.'" type="text/css" />' . "\n";
      }
    }
  }
  else {
    if (empty($assets)) {
      $html = get_assets(array('style.css'));
    }
    else {
      $html = get_assets($assets);
    }
  }
  return $html;
}

function get_javascripts($assets = array()) {
  $html = '';

  if( $_SERVER['SERVER_NAME'] == MB_DEV_SERVER_NAME && !isset($_GET["prod"])) {
    if (empty($assets)) {
      $html = file_get_contents('http://'.MB_ASSETS_URL.'/javascripts');
    }
    else {
      foreach ($assets as $asset) {
        $html .= '<script src="//'.MB_ASSETS_URL.'/assets/'.$asset.'" type="text/javascript"></script>' . "\n";
      }
    }
  }
  else {
    if (empty($assets)) {
      $html = get_assets(array('application.js'));
    }
    else {
      $html = get_assets($assets);
    }
  }
  return $html;
}

function get_assets($assets) {
  $str = file_get_contents(get_template_directory()."/assets/manifest.json");
  $json = json_decode($str,true);

  $html = '';
  foreach ($assets as $asset) {
    $ext = get_file_extension($asset);
    if ($ext == 'css' ) {
      $html .= '<link rel="stylesheet" href="'.get_bloginfo( 'template_directory' ).'/assets/'.$json['assets'][$asset].'" type="text/css" />' . "\n";
    }
    elseif ( $ext == 'js' ) {
      $html .= '<script src="'.get_bloginfo( 'template_directory' ).'/assets/'.$json['assets'][$asset].'" type="text/javascript"></script>' . "\n";
    }
  }
  return $html;
}

function nifes_get_asset_url($name) {
  $str = file_get_contents(get_template_directory()."/assets/manifest.json");
  $json = json_decode($str,true);
  return get_template_directory_uri()."/assets/" . $json['assets'][$name];
}

function image_tag($name, $classes = "", $echo = true) {
  $image_tag = '<img src="http://' . MB_ASSETS_URL . '/images/' . $name . '"'.($classes!="" ? ' class="'.$classes.'"': '').' alt="'.$name.'" />';
  if ( $echo)
    echo $image_tag;
  else
    return $image_tag;
}

/*-------------------------------------------------------------------------------------------*/
/* Helper functions */
/*-------------------------------------------------------------------------------------------*/
function str_left($str, $length) {
  return substr($str, 0, $length);
}

function str_right($str, $length) {
  return substr($str, -$length);
}

function get_file_extension($file_name) {
  return substr(strrchr($file_name,'.'),1);
}

function get_head_title() {
  if( is_front_page())
    $html = get_bloginfo('title' )." - ".get_bloginfo('description' );
  else
    $html = wp_title( ' - ', false, 'right' ) . get_bloginfo("title");
  return $html;
}

?>